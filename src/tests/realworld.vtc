varnishtest "cache-efficient brotli support"

server s1 {
	# standard gzip test
	rxreq
	expect req.url == "/"
	expect req.http.Accept-Encoding == "gzip"
	txresp -hdr "Vary: Accept-Encoding" \
	       -gziplevel 9 \
	       -gzipbody "The gzip response takes more space than the plain one"

	rxreq
	expect req.url == "/brfirst"
	expect req.http.Accept-Encoding == "gzip"
	txresp -hdr "Vary: Accept-Encoding" \
	       -gziplevel 9 \
	       -gzipbody "The gzip response takes more space than the plain one"

	# no CE br first
	rxreq
	expect req.url == "/nocebr"
	expect req.http.Accept-Encoding == "gzip"
	txresp -body "Plain as flour"

	# no CE gzip first
	rxreq
	expect req.url == "/nocegzip"
	expect req.http.Accept-Encoding == "gzip"
	txresp -body "Plain as flour"

	# compressible br first
	rxreq
	expect req.url == "/compbr"
	expect req.http.Accept-Encoding == "gzip"
	txresp -hdr "Content-Type: text/plain" \
	    -body {Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.}

	# compressible gzip first
	rxreq
	expect req.url == "/compgzip"
	expect req.http.Accept-Encoding == "gzip"
	txresp -hdr "Content-Type: text/plain" \
	    -body {Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.}

} -start

# vary: our three vairants are:
#
#		Accept-Encoding	V-A-E
# plain 	undef			undef
# gzip		gzip			undef
# bt		gzip			br
#
# we prefer br if the client supports it
#
# the primary cache object is always the gzip variant, we add the br
# variant through a recursive request back to ourself

varnish v1 \
    -arg "-a vtc=${tmpdir}/vtc.sock" \
    -arg "-a self_plain=${tmpdir}/self_plain.sock,mode=666" \
    -arg "-p vsl_mask=+VfpAcct" \
    -vcl+backend {
	import std;
	import ${vmod_brotli};

	backend self_plain {
		.path = "${tmpdir}/self_plain.sock";
	}

	sub br_recv {
		if (local.socket == "self_plain") {
			unset req.http.V-A-E;
			unset req.http.O-A-E;
			set req.hash_ignore_busy = true;
			if (req.http.Is-Bgfetch == "true") {
				set req.grace = 0s;
			}
		} else if (req.restarts == 0) {
			unset req.http.V-A-E;
			unset req.http.O-A-E;
			if (req.http.Accept-Encoding) {
				set req.http.O-A-E = req.http.Accept-Encoding;
			}
			if (req.http.Accept-Encoding ~ "\bbr\b") {
				set req.http.V-A-E = "br";
			}
		}
	}

	sub br_backend_fetch {
		# for uncacheable requests, varnish has not fiddled
		# A-E, so we see the original
		if (! bereq.uncacheable && bereq.http.V-A-E) {
			if (bereq.http.V-A-E != "br") {
				return (abandon);
			}
			unset bereq.http.O-A-E;
			unset bereq.http.Accept-Encoding;
			set bereq.http.Is-Bgfetch = bereq.is_bgfetch;
			set bereq.backend = self_plain;
			return (fetch);
		}
	}

	sub br_backend_response {
		if (bereq.backend == self_plain) {
			if (beresp.http.Vary !~ "(?i)\bAccept-Encoding\b") {
				# the other cache copy is it
				set beresp.ttl = 0s;
				set beresp.grace = 0s;
				set beresp.keep = 0s;
				return (deliver);
			}
			if (beresp.http.Content-Encoding ||
			    beresp.http.Vary !~ "\bV-A-E\b") {
				# assertion failure
				return (abandon);
			}
			set beresp.filters = "br";
			if (beresp.http.X-TTL) {
				set beresp.ttl =
				    std.duration(beresp.http.X-TTL + "s", 1s);
				unset beresp.http.X-TTL;
			} else {
				set beresp.uncacheable = true;
				if (beresp.ttl < 10s) {
				    set beresp.ttl = 10s;
				}
			}
			return (deliver);
		}
		if (beresp.http.Content-Type ~ "^text/|^application/(?:(?:x-)?javascript|json|x?html|xml|rss)|^image/(?:svg|x-icon)") {
			set beresp.do_gzip = true;
			if (! beresp.http.Vary) {
				set beresp.http.Vary = "Accept-Encoding";
			} else if (beresp.http.Vary !~ "(?i)\bAccept-Encoding\b") {
				set beresp.http.Vary = beresp.http.Vary  +
				    ", Accept-Encoding";
			}
		}
		if (beresp.http.Vary ~ "(?i)\bAccept-Encoding\b") {
			set beresp.http.Vary = beresp.http.Vary +
			    ", V-A-E";
		}
	}

	sub br_deliver {
		unset resp.http.X-TTL;
		# for ESI:
		# if (objesi.is_esi() || obj.uncacheable)
		if (obj.uncacheable) {
			# NOP
		} else if (local.socket == "self_plain") {
			set resp.http.X-TTL = obj.ttl;
		} else {
			if (resp.http.Vary ~ "\bV-A-E\s*,") {
				set resp.http.Vary = regsub(resp.http.Vary,
				    "\bV-A-E\s*,\s*", "");
			} else if (resp.http.Vary ~ ",\s*V-A-E\b") {
				set resp.http.Vary = regsub(resp.http.Vary,
				    "\s*,\s*V-A-E\b", "");
			}
		}
	}

	# --

	sub vcl_recv {
		call br_recv;
	}
	sub vcl_backend_fetch {
		call br_backend_fetch;
	}
	sub vcl_backend_response {
		call br_backend_response;
	}
	sub vcl_deliver {
		call br_deliver;
		set resp.http.gzip = req.can_gzip;
	}
} -start

client c1 -connect ${tmpdir}/vtc.sock {
	txreq -hdr "Accept-Encoding: gzip, deflate"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == "gzip"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 69

	txreq -hdr "Accept-Encoding: gzip;q=0.7, *;q=0"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == "gzip"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 69

	txreq -hdr "Accept-Encoding: deflate;q=0.7, *;q=0"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 53

	txreq -hdr "Accept-Encoding: x-gzip;q=0.4, gzip"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == "gzip"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 69

	txreq -hdr "Accept-Encoding: gzip;q=0, x-gzip"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == "gzip"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 69

	txreq -hdr "Accept-Encoding: gzip;q=0"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 53

	txreq -hdr "Accept-Encoding: gzip;q=-1"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 53

	txreq -hdr "Accept-Encoding: gzip;q=0.0000001"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == "gzip"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 69

	txreq
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 53

	txreq -hdr "Accept-Encoding: br"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == "br"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 53

	txreq -hdr "Accept-Encoding: br, gzip"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == "br"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 53

	## first request to br

	txreq -url "/brfirst" \
	    -hdr "Accept-Encoding: br"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == "br"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 53

	txreq -url "/brfirst" \
	    -hdr "Accept-Encoding: br, gzip"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == "br"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 53

	txreq -url "/brfirst" \
	    -hdr "Accept-Encoding: gzip, deflate"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == "gzip"
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 69

	txreq -url "/brfirst"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 53

	## no CE br
	txreq -url "/nocebr" \
	      -hdr "Accept-Encoding: br"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == <undef>
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 14

	txreq -url "/nocebr" \
	      -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == <undef>
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 14

	## no CE gzip first
	txreq -url "/nocegzip" \
	      -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == <undef>
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 14

	txreq -url "/nocegzip" \
	      -hdr "Accept-Encoding: br"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == <undef>
	expect resp.http.Content-Type == <undef>
	expect resp.bodylen == 14

	## compressible br
	txreq -url "/compbr" \
	      -hdr "Accept-Encoding: br"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == br
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == text/plain
	expect resp.bodylen == 149

	txreq -url "/compbr" \
	      -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == gzip
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == text/plain
	expect resp.bodylen == 191

	txreq -url "/compbr"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == text/plain
	expect resp.bodylen == 269

	## compressible gzip first
	txreq -url "/compgzip" \
	      -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.gzip == "true"
	expect resp.http.Content-Encoding == gzip
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == text/plain
	expect resp.bodylen == 191

	txreq -url "/compgzip" \
	      -hdr "Accept-Encoding: br"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == br
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == text/plain
	expect resp.bodylen == 149

	txreq -url "/compgzip"
	rxresp
	expect resp.http.gzip == "false"
	expect resp.http.Content-Encoding == <undef>
	expect resp.http.Vary == Accept-Encoding
	expect resp.http.Content-Type == text/plain
	expect resp.bodylen == 269
} -run
